﻿using multiThreadingPr.Infrastructure;

var pr = new Processor();
pr.ProcInf();
pr.MemInf();
pr.Process();

Console.WriteLine("-------------------------------------");

var pr2 = new Processor(1_000_000, 10);
pr2.Process();
Console.WriteLine("-------------------------------------");

var pr3 = new Processor(10_000_000, 10);
pr3.Process();