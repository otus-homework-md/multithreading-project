﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Management;
using System.Text;
using System.Threading.Tasks;

namespace multiThreadingPr.Infrastructure
{
    internal class Processor : IContoller,IDisposable
    {
        int[] ArrayToSum { get; set; }
        int ArrayDimension { get; set; }
        int ThreadNumbers { get; set; }
        int Sum;  
        int ChunckSize { get; set; }
        CountdownEvent cde { get; set; }


        Stopwatch sw=new Stopwatch();

        public Processor(int arrayDimension=100_000, int threadNumbers=10)
        {            
            ArrayDimension = arrayDimension;
            ThreadNumbers = threadNumbers;
            ArrayToSum = new int[ArrayDimension];
            cde = new CountdownEvent(ThreadNumbers);
            ChunckSize= arrayDimension/threadNumbers;
            
        }   

        public void Process()
        {
            //ProcInf();
            //MemInf();
            GenerateArray();
            SimpleSum();
            ThreadSum();
            LinqSum();
        }

        void GenerateArray()
        {
            var rand=new Random();
            sw.Restart();
            for(int i=0; i< ArrayDimension; i++)
            {
                ArrayToSum[i]=rand.Next(10,100);
            }
            sw.Stop();
            Console.WriteLine($"ArrayToSum[{ArrayDimension}] have generated in {sw.ElapsedMilliseconds}ms");

        }

        void SimpleSum()
        {
            Sum = 0;
            sw.Restart();
            for(var i=0; i< ArrayDimension; i++) { Sum=Sum + ArrayToSum[i]; }
            sw.Stop();
            Console.WriteLine($"ArrayToSum[{ArrayDimension}] have processed item by item in {sw.ElapsedMilliseconds}ms. Sum is {Sum}");
        }

        void ThreadSum()
        {
            Interlocked.Exchange(ref Sum, 0);
            sw.Restart();
            for (var i = 0; i <ThreadNumbers ; i++) 
            { 
                Thread thread = new Thread(DoWorkThreaSum);
                thread.Start(i*ChunckSize);
            }
            cde.Wait();
            sw.Stop();
            Console.WriteLine($"ArrayToSum[{ArrayDimension}] have processed by {ThreadNumbers} threada in {sw.ElapsedMilliseconds}ms. Sum is {Sum}");

        }
        void DoWorkThreaSum(object? obj)
        {
            var start = (int)obj;
            for(int i=start; i< start+ChunckSize; i++)
            {
                Interlocked.Add(ref Sum, ArrayToSum[i]);
            }
            cde.Signal();
        }

        void LinqSum()
        {
            Interlocked.Exchange(ref Sum, 0);
            sw.Restart();

            List<List<int>> ls = new List<List<int>>();

            for(int i=0; i<ThreadNumbers; i++)
            {
                List<int> list = new List<int>();
                for(int j=i*ChunckSize; (j<(i+1)*ChunckSize)&&(j<ArrayDimension); j++)
                {
                    list.Add(ArrayToSum[j]);
                }
                ls.Add(list);
            }

            Sum = ls.AsParallel().Select(a => SumList(a)).Aggregate((x, y) => x + y);

            sw.Stop();

            Console.WriteLine($"ArrayToSum[{ArrayDimension}] have processed by LINQ ASPARALLLEL {ThreadNumbers} chunks in {sw.ElapsedMilliseconds}ms. Sum is {Sum}");

        }

        private int  SumList(List<int> a)
        {
            return a.Sum();
        }

        public void Dispose()
        {
            cde.Dispose();
        }

        public void ProcInf()
        {
            ManagementObjectSearcher searcher8 =new ManagementObjectSearcher("root\\CIMV2","SELECT * FROM Win32_Processor");

            foreach (ManagementObject queryObj in searcher8.Get())
            {
                Console.WriteLine("------------- Win32_Processor instance ---------------");
                Console.WriteLine("Name: {0}", queryObj["Name"]);
                Console.WriteLine("NumberOfCores: {0}", queryObj["NumberOfCores"]);
                Console.WriteLine("ProcessorId: {0}", queryObj["ProcessorId"]);
                Console.WriteLine("------------------------------------------------------");
            }
        }

        public void MemInf()
        {
            ManagementObjectSearcher searcher12 = new ManagementObjectSearcher("root\\CIMV2", "SELECT * FROM Win32_PhysicalMemory");

            Console.WriteLine("------------- Win32_PhysicalMemory instance --------");
            foreach (ManagementObject queryObj in searcher12.Get())
            {
                Console.WriteLine("BankLabel: {0} ; Capacity: {1} Gb; Speed: {2} ", queryObj["BankLabel"],
                                  Math.Round(System.Convert.ToDouble(queryObj["Capacity"]) / 1024 / 1024 / 1024, 2),
                                   queryObj["Speed"]);
            }
            Console.WriteLine("------------------------------------------------------");
        }
    }
}
